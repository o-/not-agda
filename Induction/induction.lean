inductive mynat : Type
| zero : mynat
| suc : mynat -> mynat

namespace hidden

open mynat

def to_mynat : nat -> mynat
| 0 := zero
| (n + 1) := suc (to_mynat n)
local prefix `$`:1000   := to_mynat

def add : mynat -> mynat -> mynat
| zero n := n
| (suc n) m := suc (add n m)
local infix ` + `:100 := add


def mul : mynat -> mynat -> mynat
| zero n := zero
| (suc m) n := n + mul m n
local infix ` * `:200 := mul

def pow : mynat -> mynat -> mynat
| n zero := $1
| n (suc m) := n * (pow n m)
local infix ` ^ `:300 := pow

def mon : mynat -> mynat -> mynat
| m zero := m
| zero (suc n) := zero
| (suc m) (suc n) := mon m n
local infix ` - `:100 := mon

lemma add_suc : ∀ (m n : mynat), m + suc(n) = suc(m + n)
| zero n      := by refl
| (suc m) n   := begin
  have: suc (m + suc n) = suc (suc (m + n)),
    rewrite add_suc,
  assumption,
end
lemma suc_add (m n : mynat) : suc(m) + n = suc(m + n) := by refl

lemma add_zero : ∀ (n : mynat), n + zero = n
| zero    := by refl
| (suc n) := begin
  have: suc (n + zero) = suc n,
    rewrite add_zero,
  assumption
end
lemma zero_add (n: mynat) : zero + n = n := by refl

theorem add_assoc (a b c : mynat) : (a + b) + c = a + (b + c) :=
begin
  induction c,
  case zero {
    rw [add_zero, add_zero]
  },
  case suc : c ih {
    rw [add_suc, add_suc, add_suc, ih],
  }
end

theorem add_comm (m n : mynat) : m + n = n + m :=
begin
  induction n,
  case zero : { rw add_zero, refl },
  case suc : n ih {
    rw add_suc,
    rw ih,
    refl
  },
end

lemma zero_mul (n : mynat) : zero * n = zero := by refl
lemma suc_mul (a b : mynat) : suc(a) * b = b + a*b := by refl

lemma mul_zero (n : mynat) : n * zero = zero :=
begin
  induction n,
  case zero { refl },
  case suc : n ih {
    have: suc (n * zero) = suc zero, rw ih,
    assumption,
  }
end

lemma mul_one (a : mynat) : a * $1 = a :=
begin
  induction a,
  case zero { refl },
  case suc : a ih {
    rw suc_mul,
    have swap: suc a = suc (a * $1), rw ih,
    rw swap,
    rw ih,
    refl
  }
end


lemma extract_one (a : mynat) : (suc a) = a + $1 :=
begin
  have eta: suc a = suc(a+zero), rw add_zero,
  rw eta,
  rw <-add_suc,
  refl
end

lemma mul_suc (a b : mynat) : a * suc(b) = a*b + a :=
begin
  induction a,
  case zero { rw zero_mul, rw zero_mul, rw zero_add },
  case suc : a ih {
    have ax: (suc a) * (suc b) = (suc b) + a * (suc b), refl,
    rw ax,
    rw ih,
    have ax2: (suc a) * b = b + a * b, refl,
    rw ax2,
    have swap1: suc b + (a * b + a) = suc b + a*b + a, rw add_assoc,
    rw swap1,
    rw [extract_one, extract_one],
    simp [add_comm, add_assoc],
  }
end


theorem add_mul_distrib (a b k : mynat) : (a + b) * k = (a * k) + (b * k) :=
begin
induction k,
case zero {rw [mul_zero, mul_zero, mul_zero, add_zero]},
case suc : k ih {
calc
  (a+b) * suc(k) = (a+b)*k + (a+b)           : by rw mul_suc
  ...            = (a*k)+(b*k)+(a+b)         : by rw ih
  ...            = (a*k) + ((b*k) + (a+b))   : by rw add_assoc
  ...            = (a*k)+ (a + (b*k) + b)    : by simp [add_comm, add_assoc]
  ...            = ((a*k)+a)+((b*k)+b)       : by {rw add_assoc, rw add_assoc}
  ...            = (a*(suc k))+(b*(suc k))   : by {rw mul_suc, rw mul_suc}
}
end

theorem mul_assoc (m n p : mynat) : (m * n) * p = m * (n * p) :=
begin
  induction m,
  case zero { rw [zero_mul, zero_mul, zero_mul] },
  case suc : m ih {
    rw [suc_mul, suc_mul],
    rw [add_mul_distrib],
    rw ih
  },
end

theorem mul_comm (m n : mynat) : m * n = n * m :=
begin
  induction m,
  case zero { rw [zero_mul, mul_zero] },
  case suc : m ih {
    rw mul_suc,
    rw <-ih,
    rw suc_mul,
    have swap: n + m * n = m * n + n, rw add_comm,
    rw swap
  }
end

lemma zero_mon (n : mynat) : zero - n = zero :=
begin
  induction n,
  case zero { refl },
  case suc : n ih { refl }
end
lemma mon_zero (n : mynat) : n - zero = n :=
begin
  induction n,
  case zero { refl },
  case suc : n ih { refl },
end

lemma mon_one : ∀ (n m : mynat), n - suc m = n - m - suc zero
| zero m          := by rw [zero_mon, zero_mon, zero_mon]
| (suc n) zero    := by refl
| (suc n) (suc m) := by rw [mon, mon_one, mon]


theorem mon_add_assoc (n m p : mynat) : m - n - p = m - (n + p) :=
begin
  induction p,
  case zero {
    simp [mon_zero, add_zero]
  },
  case suc : p ih {
    rw mon_one,
    rw ih,
    rw <-mon_one,
    rw add_suc,
  }
end

lemma pow_suc (n m : mynat) : n ^ suc(m) = n * n ^ m := by simp [pow]

theorem pow_add_dist (n m p : mynat) : m ^ (n + p) = (m ^ n) * (m ^ p) :=
begin
  induction p,
  case zero {
    simp [pow, add_zero],
    rw mul_one
  },
  case suc : p ih {
    rw pow_suc,
    have swap: m ^ n * (m * m ^ p) = m ^ n * m ^ p * m, simp [mul_comm, mul_assoc],
    rw [swap, <-ih, mul_comm, <-pow_suc, add_suc],
  }
end

theorem mul_pow_dist (n m p : mynat) : (m * n) ^ p = (m ^ p) * (n ^ p) :=
begin
  induction p,
  case zero {
    simp [pow],
    rw mul_one
  },
  case suc : p ih {
    rw [pow_suc, pow_suc, pow_suc, ih],
    calc
      m * n * (m^p * n^p)    = m * (n * m^p) * n^p   : by simp [mul_assoc]
      ...                    = m * (m^p * n) * n^p   : by simp [mul_comm]
      ...                    = m * m^p * n * n^p     : by simp [mul_assoc]
      ...                    = m * m^p * (n * n^p)   : by simp [mul_assoc]
  }
end

theorem pow_mul_assoc (n m p : mynat) :  m ^ (n * p) = (m ^ n) ^ p :=
begin
  induction p,
  case zero {
    simp [pow, mul_zero]
  },
  case suc : p ih {
    rw [pow_suc, <-ih, mul_suc, pow_add_dist, mul_comm],
  }
end

end hidden
