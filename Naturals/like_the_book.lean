inductive mynat : Type
| zero : mynat
| suc : mynat -> mynat

open mynat

def to_mynat : nat -> mynat
| 0 := zero
| (n + 1) := suc (to_mynat n)

local prefix `$`:1000   := to_mynat


def add : mynat -> mynat -> mynat
| zero n := n
| (suc n) m := suc (add n m)
local infix ` + `:100 := add

#check zero
#check suc (suc zero)
#check zero + suc (suc zero)
#check $2 + $3
#check $2 + $3 = $5
#check suc ($0 + $3)
#check add zero
#check add (suc zero)
#check add ($0)


theorem t1 : $2 + $3 = $5 :=
begin
have: suc ($1 + $3) = $5,
have: suc (suc (($0) + ($3))) = $5,
have: suc (suc ($3)) = $5,
refl,
assumption,
assumption,
assumption
end

def mul : mynat -> mynat -> mynat
| zero n := zero
| (suc m) n := n + mul m n
local infix ` * `:200 := mul

theorem t2 : $2 * $3 = $6 :=
begin
have: $3 + $1 * $3 = $6,
have: $3 + $3 + $0 * $3 = $6,
have: $3 + $3 + $0 = $6,
refl,
repeat {assumption}
end

def pow : mynat -> mynat -> mynat
| n zero := $1
| n (suc m) := n * (pow n m)
local infix ` ^ `:300 := pow

theorem t3 : $3 ^ $4 = $81 :=
begin
have: $3 * $3 ^ $3 = $81,
have: $3 * $3 * $3 ^ $2 = $81,
have: $3 * $3 * $3 * $3 ^ $1 = $81,
have: $3 * $3 * $3 * $3 * $1 = $81,
refl,
repeat {assumption}
end

def mon : mynat -> mynat -> mynat
| m zero := m
| zero (suc n) := zero
| (suc m) (suc n) := mon m n
local infix ` - `:50 := mon

theorem t4 : $3 - $2 = $1 :=
begin
have: $2 - $1 = $1,
have: $1 - $0 = $1,
refl,
repeat {assumption}
end

theorem assoc
  (m n p : mynat) :
    (m + n) + p = m + (n + p) :=
begin
  induction m,
  case zero : { refl },
  case suc : m ih {
    let lhs := suc m + n + p,
    have: lhs = suc (m + n) + p, trivial,
    have: lhs = suc (m + n + p), assumption,
    have: lhs = suc (m + (n + p)), cc,
    assumption
  },
end

lemma zadd (m : mynat) : m + zero = m :=
begin
  induction m,
  case zero : { trivial },
  case suc : m ih {
    have: suc m + zero = suc (m + zero), trivial,
    cc
  },
end

lemma sadd (m n : mynat) : m + suc n = suc (m + n) :=
begin
  induction m,
  case zero : { trivial },
  case suc : m ih {
    let rhs := suc m + suc n,
    have: rhs = suc (m + suc n), trivial,
    have: rhs = suc (suc (m + n)), cc,
    have: rhs = suc (suc m + n), assumption,
    assumption
  },
end

theorem comm
  (m n : mynat) :
    m + n = n + m :=
begin
  induction n,
  case zero : { rewrite zadd, trivial },
  case suc : n ih {
    have: m + suc n = suc (m + n), rw sadd,
    have: m + suc n = suc (n + m), cc,
    assumption 
  },
end


theorem rearrange (m n p q : mynat) : (m + n) + (p + q) = m + (n + p) + q :=
begin
repeat {rw assoc},
end
