inductive Bin : Type
| bin   : Bin 
| O    : Bin -> Bin 
| I    : Bin -> Bin 
notation `b[` l:(foldl `, ` (h t, h (t)) Bin.bin) `]` := l

open Bin 
#check b[I, O, O]

def inc : Bin -> Bin 
| bin := I bin 
| (O x) := I x 
| (I x) := O (inc(x))

theorem t5 : inc bin = I bin := by refl
theorem t6 : inc b[O] = b[I] := by refl
theorem t7 : inc b[I] = b[I,O] := by refl
theorem t8 : inc b[I,O] = b[I,I] := by refl
theorem t9 : inc b[I,I] = b[I,O,O] := by refl
theorem t10 : inc b[I,O,I,I] = b[I,I,O,O] := by refl

