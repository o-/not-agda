inductive mynat : Type
| zero : mynat
| suc : mynat -> mynat

namespace hidden

open mynat

def to_mynat : nat -> mynat
| 0 := zero
| (n + 1) := suc (to_mynat n)
local prefix `$`:1000   := to_mynat

def add : mynat -> mynat -> mynat
| zero n := n
| (suc n) m := suc (add n m)
local infix ` + `:100 := add


def mul : mynat -> mynat -> mynat
| zero n := zero
| (suc m) n := n + mul m n
local infix ` * `:200 := mul

def pow : mynat -> mynat -> mynat
| n zero := $1
| n (suc m) := n * (pow n m)
local infix ` ^ `:300 := pow

def mon : mynat -> mynat -> mynat
| m zero := m
| zero (suc n) := zero
| (suc m) (suc n) := mon m n
local infix ` - `:100 := mon

lemma add_suc (m n : mynat) : m + suc(n) = suc(m + n) :=
begin
induction m,
case zero { refl },
case suc : m ih {
  have: suc (m + suc n) = suc (suc (m + n)), rewrite ih, assumption
}
end
lemma suc_add (m n : mynat) : suc(m) + n = suc(m + n) := by refl

lemma add_zero (n : mynat) : n + zero = n :=
begin
  induction n,
  case zero { refl },
  case suc : n ih {
    have: suc (n + zero) = suc n, rewrite ih,
    assumption
  }
end
lemma zero_add (n: mynat) : zero + n = n := by refl

end hidden
